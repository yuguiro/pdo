<?php

require "../conexion.php";
require "../common.php";

if (isset($_POST['submit'])) {
  if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

  try {
    $conexion = new PDO($dsn, $usuario, $contraseña);

    $user =[
      "id"        => $_POST['id'],
      "apellido_paterno" => $_POST['apellido_paterno'],
      "apellido_materno" => $_POST['apellido_materno'],
      "nombres"  => $_POST['nombres'],
      "email"     => $_POST['email'],
      "edad"       => $_POST['edad'],
      "procedencia"  => $_POST['procedencia'],
      "fecha"      => $_POST['fecha']
    ];

    $sql = "UPDATE usuarios 
            SET id = :id, 
              apellido_paterno = :apellido_paterno, 
              apellido_materno = :apellido_materno, 
              nombres = :nombres, 
              email = :email, 
              edad = :edad, 
              procedencia = :procedencia, 
              fecha = :fecha 
            WHERE id = :id";
  
  $statement = $conexion->prepare($sql);
  $statement->execute($user);
  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
  }
}
  
if (isset($_GET['id'])) {
  try {
    $conexion = new PDO($dsn, $usuario, $contraseña);
    $id = $_GET['id'];

    $sql = "SELECT * FROM usuarios WHERE id = :id";
    $statement = $conexion->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();
    
    $user = $statement->fetch(PDO::FETCH_ASSOC);
  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
  }
} else {
    echo "¡Algo salió mal!";
    exit;
}
?>

<?php require "templates/header.php"; ?>

<?php if (isset($_POST['submit']) && $statement) : ?>
 <blockquote><?php echo escape($_POST['apellido_paterno']); ?> Actualizado exitosamente.</blockquote>
<?php endif; ?>

<h2>Editar Usuario</h2>

<form method="post">
    <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">
    <?php foreach ($user as $key => $value) : ?>
      <label for="<?php echo $key; ?>"><?php echo ucfirst($key); ?></label>
     <input type="text" name="<?php echo $key; ?>" id="<?php echo $key; ?>" value="<?php echo escape($value); ?>" <?php echo ($key === 'id' ? 'readonly' : null); ?>>
    <?php endforeach; ?> 
    <input type="submit" name="submit" value="Submit">
</form>

<a href="index.php">Regresar al inicio</a>

<?php require "templates/footer.php"; ?>
