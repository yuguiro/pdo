<?php

require "../conexion.php";
require "../common.php";

try {
    $conexion = new PDO($dsn, $usuario, $contraseña);

    $sql = "SELECT * FROM usuarios";

    $statement = $conexion->prepare($sql);
    $statement->execute();

    $result = $statement->fetchAll();
} catch (PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
}
?>
<?php require "templates/header.php"; ?>
 <link rel="stylesheet" href="css/actualizar.css">
<h2>Actualizar tabla usuarios</h2>

<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Nombres</th>
            <th>Email</th>
            <th>Edad</th>
            <th>Procedencia</th>
            <th>Fecha</th>
            <th>Editar</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($result as $row) : ?>
            <tr>
                <td><?php echo escape($row["id"]); ?></td>
                <td><?php echo escape($row["apellido_paterno"]); ?></td>
                <td><?php echo escape($row["apellido_materno"]); ?></td>
                <td><?php echo escape($row["nombres"]); ?></td>
                <td><?php echo escape($row["email"]); ?></td>
                <td><?php echo escape($row["edad"]); ?></td>
                <td><?php echo escape($row["procedencia"]); ?></td>
                <td><?php echo escape($row["fecha"]); ?> </td>
                <td><a href="base-actualizar.php?id=<?php echo escape($row["id"]); ?>">Editar</a></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<a href="index.php">Regresar al menu inicio</a>

<?php require "templates/footer.php"; ?>
