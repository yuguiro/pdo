<?php require "../conexion.php"; ?>
<?php require "../common.php"; ?>

<?php
if (isset($_POST['submit'])) {
  if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

  try  {
    $connection = new PDO($dsn, $usuario, $contraseña);

    $sql = "SELECT * 
            FROM usuarios
            WHERE procedencia = :procedencia";

    $procedencia = $_POST['procedencia'];
    $statement = $connection->prepare($sql);
    $statement->bindParam(':procedencia', $procedencia, PDO::PARAM_STR);
    $statement->execute();

    $result = $statement->fetchAll();
  } catch(PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
  }
}
?>

<?php require "templates/header.php"; ?>

<h2>Encontrar usuario basado en procedencia</h2>
<?php
try {
  $connection = new PDO($dsn, $usuario, $contraseña);

  $sql = "SELECT DISTINCT procedencia FROM usuarios";

  $statement = $connection->prepare($sql);
  $statement->execute();

  $lugares = $statement->fetchAll(PDO::FETCH_COLUMN);
} catch(PDOException $error) {
  echo $sql . "<br>" . $error->getMessage();
}
?>

<form method="post">
  <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">
  <label for="procedencia">Procedencia</label>
  <select id="procedencia" name="procedencia">
    <option value="">LUGAR</option>
    <?php foreach ($lugares as $lugar) : ?>
      <option value="<?php echo escape($lugar); ?>"><?php echo escape($lugar); ?></option>
    <?php endforeach; ?>
  </select>       
  <input type="submit" name="submit" value="Ver resultados">
</form>
<?php  
if (isset($_POST['submit'])) {
  if ($result && $statement->rowCount() > 0) { ?>
    <h2>Resultados</h2>

    <table>
      <thead>
        <tr>
          <th>#</th>
          <th>Apellido Paterno</th>
          <th>Apellido Materno</th>
          <th>Nombres</th>
          <th>Email</th>
          <th>Edad</th>
          <th>Procedencia</th>
          <th>Fecha</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($result as $row) : ?>
        <tr>
          <td><?php echo escape($row["id"]); ?></td>
          <td><?php echo escape($row["apellido_paterno"]); ?></td>
          <td><?php echo escape($row["apellido_materno"]); ?></td>
          <td><?php echo escape($row["nombres"]); ?></td>
          <td><?php echo escape($row["email"]); ?></td>
          <td><?php echo escape($row["edad"]); ?></td>
          <td><?php echo escape($row["procedencia"]); ?></td>
          <td><?php echo escape($row["fecha"]); ?></td>
        </tr>
      <?php endforeach; ?>
      </tbody>
    </table>
  <?php } else { ?>
    <blockquote>No se encontraron resultados para <?php echo escape($_POST['procedencia']); ?>.</blockquote>
  <?php } 
} ?> 

<a href="index.php">Regresar al inicio</a>
<link rel="stylesheet" href="css/leer.css">

<?php require "templates/footer.php"; ?>
